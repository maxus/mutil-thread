package com.yrxy.thread.case5;

import java.util.Map;
import java.util.Queue;

/**
 * @className Worker
 */
public class Worker implements Runnable {

	protected Queue<Object> workQueue;

	protected Map<String, Object> resultMap;

	public void setWorkQueue(Queue<Object> workQueue) {
		this.workQueue = workQueue;
	}

	public void setResultMap(Map<String, Object> resultMap) {
		this.resultMap = resultMap;
	}

	/**
	 * 计算相关的逻辑
	 */
	public Object handle(Object input) {
		return input;
	}

	@Override
	public void run() {
		while (true) {
			//拉取内容
			Object input = workQueue.poll();
			if (input == null) {
				break;
			}
			Object result = handle(input);

			// 计算的逻辑结果放到这个map里面去.
			resultMap.put(Integer.toString(input.hashCode()), result);
		}
	}
}
