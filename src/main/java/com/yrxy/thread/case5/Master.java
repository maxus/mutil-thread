package com.yrxy.thread.case5;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Master是协调用的,给计算任务分给不同的worker线程来处理.
 *
 */
public class Master {
	// 放任务的队列
	protected Queue<Object> workQueue = new ConcurrentLinkedQueue<Object>();
	//装载Worker线程
	protected Map<String, Thread> workerThreadMap = new HashMap<String, Thread>();
	//每个worker计算结果放到这个map里面
	protected Map<String, Object> resultMap = new ConcurrentHashMap<String, Object>();


	public boolean isComplete() {
//		会判断每一个线程状态是否有结束,如果没有结束就会继续等待
		for (Map.Entry<String, Thread> entry : workerThreadMap.entrySet()) {
			if (entry.getValue().getState() != Thread.State.TERMINATED) {
				return false;
			}
		}
		return true;
	}


	public Master(Worker worker, int countWorker) {
		worker.setWorkQueue(workQueue);
		worker.setResultMap(resultMap);
		for (int i = 0; i < countWorker; i++) {
			workerThreadMap.put(Integer.toString(i), new Thread(worker, Integer.toString(i)));
		}
	}

	//提交
	public void submit(Object job) {
		workQueue.add(job);
	}


	public Map<String, Object> getResultMap() {
		return resultMap;
	}

	//发起执行
	public void execute() {
		for (Map.Entry<String, Thread> entry : workerThreadMap.entrySet()) {
			entry.getValue().start();
		}
	}
}
