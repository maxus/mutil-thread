package com.yrxy.thread.case5;

/**
 * @author root
 * @className ComputeWorker
 */
public class ComputeWorker extends Worker {
	/**
	 * 模拟校验逻辑和计算逻辑
	 */
	@Override
	public Object handle(Object input) {
		try {
			//模拟执行校验逻辑
			System.out.println("我开始校验了");
			Thread.sleep(5);
			System.out.println("我校验完了");
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		Long i = (Long) input;
		return i * i;
	}
}
