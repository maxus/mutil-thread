package com.yrxy.thread.case5;

import java.util.Map;
import java.util.Set;

import org.junit.Test;

/**
 * @className MultiThreadCompute
 */
public class MultiThreadCompute {

	@Test
	public void testCompute() {
		Long start = System.currentTimeMillis();
		// 初始化Master,初始化100个Worker
		Master master = new Master(new ComputeWorker(), 100);
		//for循环提交任务
		for (long i = 1; i < 1000001; i++) {
			master.submit(i);
		}
		master.execute();

		long re = 0;
		//获取计算结果
		Map<String, Object> resultMap = master.getResultMap();
		//如不满足这个while结果之后,说明计算完成了
		while (resultMap.size() > 0 || !master.isComplete()) {
			Set<String> keys = resultMap.keySet();
			String key = null;
			for (String k : keys) {
				key = k;
				break;
			}
			Long singleResult = null;
			if (key != null) {
				singleResult = (Long) resultMap.get(key);
			}
			if (singleResult != null) {
				re += singleResult;
			}
			if (key != null) {
				resultMap.remove(key);
			}
		}


		Long end = System.currentTimeMillis();

		System.out.println("multi thread ,testMasterWorker:" + re);
		System.out.println("multi thread ,process  time is : " + (end - start)
				+ "  ms");
	}


}
